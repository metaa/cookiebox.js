export default interface IStringString {
	[key: string]: string
}
